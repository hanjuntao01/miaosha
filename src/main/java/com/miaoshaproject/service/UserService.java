package com.miaoshaproject.service;

import com.miaoshaproject.error.BusinessException;
import com.miaoshaproject.model.UserModel;

public interface UserService {
    UserModel getUserById(Integer id);
    UserModel register(UserModel userModel) throws BusinessException;
    UserModel validataLogin(String telphone, String encrptPassword) throws BusinessException;
}
