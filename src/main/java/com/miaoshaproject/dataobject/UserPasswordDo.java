package com.miaoshaproject.dataobject;

/**
 * 描述:user_password表的实体类
 * @version
 * @author:  Hanhan
 * @创建时间: 2019-07-29
 */
public class UserPasswordDo {
    /**
     * 
     */
    private Integer id;

    /**
     * 
     */
    private String encrptPassword;

    /**
     * 
     */
    private Integer userId;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return encrpt_password 
     */
    public String getEncrptPassword() {
        return encrptPassword;
    }

    /**
     * 
     * @param encrptPassword 
     */
    public void setEncrptPassword(String encrptPassword) {
        this.encrptPassword = encrptPassword == null ? null : encrptPassword.trim();
    }

    /**
     * 
     * @return user_id 
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId 
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}