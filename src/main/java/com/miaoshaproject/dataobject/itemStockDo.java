package com.miaoshaproject.dataobject;

/**
 * 描述:item_stock表的实体类
 * @version
 * @author:  Hanhan
 * @创建时间: 2019-07-30
 */
public class itemStockDo {
    /**
     * 
     */
    private Integer id;

    /**
     * 
     */
    private Integer stock;

    /**
     * 
     */
    private Integer itemId;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return stock 
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * 
     * @param stock 
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * 
     * @return item_id 
     */
    public Integer getItemId() {
        return itemId;
    }

    /**
     * 
     * @param itemId 
     */
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }
}