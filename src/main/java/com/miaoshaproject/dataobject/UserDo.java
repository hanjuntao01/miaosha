package com.miaoshaproject.dataobject;

/**
 * 描述:user_info表的实体类
 * @version
 * @author:  Hanhan
 * @创建时间: 2019-07-29
 */
public class UserDo {
    /**
     * 
     */
    private Integer id;

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    private Byte gender;

    /**
     * 
     */
    private Integer age;

    /**
     * 
     */
    private String telphone;

    /**
     * 
     */
    private String registerMode;

    /**
     * 
     */
    private String thridPartyId;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return name 
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name 
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 
     * @return gender 
     */
    public Byte getGender() {
        return gender;
    }

    /**
     * 
     * @param gender 
     */
    public void setGender(Byte gender) {
        this.gender = gender;
    }

    /**
     * 
     * @return age 
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 
     * @param age 
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 
     * @return telphone 
     */
    public String getTelphone() {
        return telphone;
    }

    /**
     * 
     * @param telphone 
     */
    public void setTelphone(String telphone) {
        this.telphone = telphone == null ? null : telphone.trim();
    }

    /**
     * 
     * @return register_mode 
     */
    public String getRegisterMode() {
        return registerMode;
    }

    /**
     * 
     * @param registerMode 
     */
    public void setRegisterMode(String registerMode) {
        this.registerMode = registerMode == null ? null : registerMode.trim();
    }

    /**
     * 
     * @return thrid_party_id 
     */
    public String getThridPartyId() {
        return thridPartyId;
    }

    /**
     * 
     * @param thridPartyId 
     */
    public void setThridPartyId(String thridPartyId) {
        this.thridPartyId = thridPartyId == null ? null : thridPartyId.trim();
    }
}