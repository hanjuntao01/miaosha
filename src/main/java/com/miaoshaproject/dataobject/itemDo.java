package com.miaoshaproject.dataobject;

import java.math.BigDecimal;

/**
 * 描述:item表的实体类
 * @version
 * @author:  Hanhan
 * @创建时间: 2019-07-30
 */
public class itemDo {
    /**
     * 
     */
    private Integer id;

    /**
     * 商品名
     */
    private String title;

    /**
     * 
     */
    private BigDecimal price;

    /**
     * 描述
     */
    private String description;

    /**
     * 
     */
    private Integer sales;

    /**
     * 
     */
    private String imgUrl;

    /**
     * 
     * @return id 
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 商品名
     * @return title 商品名
     */
    public String getTitle() {
        return title;
    }

    /**
     * 商品名
     * @param title 商品名
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 
     * @return price 
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 
     * @param price 
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 描述
     * @return description 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 描述
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * 
     * @return sales 
     */
    public Integer getSales() {
        return sales;
    }

    /**
     * 
     * @param sales 
     */
    public void setSales(Integer sales) {
        this.sales = sales;
    }

    /**
     * 
     * @return img_url 
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     * 
     * @param imgUrl 
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }
}