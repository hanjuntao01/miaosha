package com.miaoshaproject.dao;

import com.miaoshaproject.dataobject.itemDo;

public interface itemDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(itemDo record);

    int insertSelective(itemDo record);

    itemDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(itemDo record);

    int updateByPrimaryKey(itemDo record);
}