package com.miaoshaproject.dao;

import com.miaoshaproject.dataobject.itemStockDo;

public interface itemStockDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(itemStockDo record);

    int insertSelective(itemStockDo record);

    itemStockDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(itemStockDo record);

    int updateByPrimaryKey(itemStockDo record);
}